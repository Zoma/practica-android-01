package com.example.practicaandroid01

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {
    var enableButton: Boolean by Delegates.observable(false){property, oldValue, newValue ->
        button.isEnabled = newValue
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cb_enable.setOnCheckedChangeListener { buttonView, isChecked ->
            enableButton = isChecked
        }
    }
}
